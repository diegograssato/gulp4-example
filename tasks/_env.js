'use strict';
var args = require('yargs').argv;
var glp = require('gulp-load-plugins')({
  DEBUG: false, // when set to true, the plugin will log info to console. Useful for bug reporting and issue debugging
  lazy: true, // whether the $ should be lazy loaded on demand

});
module.exports = {

  isProduction: !!glp.util.env.production,
  source: (glp.util.env.production ? 'dist' : 'src'),
  project: (args.project || 'admin')

};
