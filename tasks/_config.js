'use strict';

var globals = require("./_env.js");

module.exports = {
  basePaths: {
    node_modules: './node_modules/',
  },
  paths: {
    scripts: {
      fileName: 'all.js',
      src: globals.project + '/src/js/',
      dest: globals.project + '/dist/js/'
    },
    styles: {
      fileName: 'all.css',
      src: globals.project + '/src/sass/',
      dest: globals.project + '/dist/css/',
    }
  }
};
