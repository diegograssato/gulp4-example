'use strict';

module.exports = function task(gulp, $, config, globals) {

  gulp.task('sync', function() {

    var sources = gulp.src([
      config.paths.scripts.src + '**/*.js',
      config.paths.styles.src + '**/*.css'
    ]);

    gulp.watch([
      config.paths.scripts.src + '**/*.js',
      config.paths.styles.src + '**/*.scss'
    ], gulp.series('jscheck', 'default'));

  });
};
