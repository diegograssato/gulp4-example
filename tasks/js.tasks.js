'use strict';

module.exports = function task(gulp, $, config, globals) {

  gulp.task('scripts', function() {

    return gulp.src(config.paths.scripts.src + '**/*.js')
      .pipe(globals.isProduction ? $.uglify() : $.sourcemaps.init())
      .pipe($.concat(config.paths.scripts.fileName))
      .pipe($.if(globals.isProduction, $.rename({
        suffix: '.min'
      })))
      .pipe($.if(!globals.isProduction, $.sourcemaps.write('.')))
      .pipe(gulp.dest(config.paths.scripts.dest));

  });

};
