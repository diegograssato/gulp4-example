'use strict';

module.exports = function task(gulp, $, config, globals) {

  gulp.task('clean', function() {

    return gulp.src([
        config.paths.scripts.dest + '**/*.js',
        config.paths.scripts.dest + '**/*.js.map',
        config.paths.styles.dest + '**/*.css',
        config.paths.styles.dest + '**/*.css.map',
      ])
      .pipe($.rimraf());

  });

};
