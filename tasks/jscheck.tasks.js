'use strict';

module.exports = function task(gulp, $, config, globals) {

  gulp.task('jscheck', function() {

    return gulp.src(config.paths.scripts.src + '**/*.js')
      .pipe($.jshint('.jshintrc'))
      .pipe($.jshint.reporter('jshint-stylish'));

  });
};
