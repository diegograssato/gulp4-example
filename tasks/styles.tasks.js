'use strict';

module.exports = function task(gulp, $, config, globals) {

  gulp.task('styles', function() {

    return gulp.src(config.paths.styles.src + '**/*.scss')
      .pipe(globals.isProduction ? $.util.noop() : $.sourcemaps.init())
      .pipe($.cssGlobbing({
        extensions: ['.scss']
      }))
      .pipe($.sass('sass', {
        style: 'expanded'
      }))
      .pipe(globals.isProduction ? $.minifyCss() : $.sourcemaps.write('.'))
      .pipe($.concat(config.paths.styles.fileName))
      .pipe($.if(globals.isProduction, $.rename({
        suffix: '.min'
      })))
      .pipe(gulp.dest(config.paths.styles.dest));

  });
};
