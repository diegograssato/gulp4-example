'use strict';

/**
Execution:

gulp --project=client
gulp --project=client --production
gulp --project=admin
gulp --project=client --production

*/
var gulp = require('gulp');
var fs = require('fs');

var glp = require('gulp-load-plugins')({
  DEBUG: false, // when set to true, the plugin will log info to console. Useful for bug reporting and issue debugging
  lazy: true, // whether the glp should be lazy loaded on demand
});

try {

  var globals = require("./tasks/_env.js");
  var config = require("./tasks/_config.js");

} catch (e) {
  throw (e.message);
}

glp.util.log('### Producao? ' + globals.isProduction);
glp.util.log('### Producao path? ' + globals.source);
glp.util.log('### Projeto? ' + globals.project);
glp.util.log('### Running the task on ' + globals.project + ' ###');
glp.loadSubtasks('tasks', glp, config, globals);

gulp.task('index', function() {
  var target = gulp.src(globals.project + '/src/index.html');

  var sources = gulp.src([
    config.paths.scripts.dest + '**/*.js',
    config.paths.styles.dest + '**/*.css'
  ]);

  return target
    .pipe(glp.inject(sources, {
      ignorePath: globals.project + "/dist"
    }))
    .pipe(glp.if(globals.isProduction, glp.htmlmin({
      collapseWhitespace: true
    })))
    .pipe(gulp.dest(globals.project + '/dist'));


});

gulp.task('default',
  gulp.series(gulp.parallel('clean'), gulp.parallel('scripts',
      'styles'),
    'index')
);
